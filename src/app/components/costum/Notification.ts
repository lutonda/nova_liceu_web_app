export class Notification {
    constructor() {

    }

  public showNotification(message, type) {
    const colors = ['', 'info', 'success', 'warning', 'danger'];

    $.notify({
        icon: 'otifications',
        message: message

    }, {
        type: colors[type],
        timer: 4000,
        placement: {
            from: 'top',
            align: 'right'
        },
    });
    }
}
