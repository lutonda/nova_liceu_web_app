import { Component, OnInit } from '@angular/core';
import { Professor } from 'app/Entity/professor';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';


import { DataService } from 'app/services/data.service';
import { Router } from '@angular/router';
import { Contact } from 'app/Entity/contact';
import { IdCardType } from 'app/Entity/idCardType';
import { Township } from 'app/Entity/township';
import { ContactType } from 'app/Entity/contactType';
import { HttpErrorResponse } from '@angular/common/http';
import { Person } from 'app/Entity/person';
import { Sex } from 'app/Entity/sex';
import { Address } from 'app/Entity/address';
import { IdCard } from 'app/Entity/idCard';

@Component({
  selector: 'app-add-professor',
  templateUrl: './add-professor.component.html',
  styleUrls: ['./add-professor.component.scss'],
})
export class AddProfessorComponent implements OnInit {

  isLoading = true;

  model = new Professor()
  contact = new Contact();
  person = new Person();
  sexs = new Array<Sex>();

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;

  contacts: Array<Contact> = [];
  idCardTypes: Array<IdCardType> = new Array<IdCardType>();
  townships: Array<Township> = new Array<Township>();
  contactTypes: Array<ContactType> = new Array<ContactType>();
  bloodGroups: Array<BloodGroup> = new Array<BloodGroup>();

  //
  constructor(private dataService: DataService, private _formBuilder: FormBuilder, private router: Router) {}

  ngOnInit() {

      this.firstFormGroup = this._formBuilder.group({
        firstName: ['', Validators.required],
        middleName: ['', Validators.required],
        lastName: ['', Validators.required],
        sex: ['', Validators.required],
        birthDate: ['', Validators.required],
      });
      this.secondFormGroup = this._formBuilder.group({
        idCard_idCardType: ['', Validators.required],
        idCard_description: ['', Validators.required],
        address_township: ['', Validators.required],
        address_description: ['', Validators.required],
        birthPlace: ['', Validators.required],
      });
      this.thirdFormGroup  = this._formBuilder.group({
        lider: ['', Validators.required]
      });

   this.dataService.get('pedagogy/professor/new').subscribe((data: any) => {
      this.townships = data.township;
      this.idCardTypes = data.idCardType;
      this.contactTypes = data.contactType;
      this.bloodGroups = data.bloodGroup;
      this.sexs = data.sex

      this.isLoading = false;
    },
    (err: HttpErrorResponse) => {
       // this.isLoginError = true;
    });
  }

  addContact() {
    this.model.person.contacts.push(this.contact);
    this.contact = new Contact();
  }
  removeContact(contact) {
    const index = this.model.person.contacts.indexOf(contact);
    this.model.person.contacts.splice(index, 1);
  }
  onSubmit() {

    this.person.idCard.idCardNumber = this.secondFormGroup.value.idCard_description;
    this.person.idCard.idCardType = this.secondFormGroup.value.idCard_idCardType;
    this.person.address.description = this.secondFormGroup.value.address_description;
    this.person.address.township = this.secondFormGroup.value.address_township;

    this.model.person = {...this.person, ...this.model, ...this.firstFormGroup.value, ...this.secondFormGroup.value}
    // this.model.period = this.periods.find(perio => perio.id === this.model.period)
    this.dataService.post(this.model, 'pedagogy/professor').subscribe((data: any) => {
       this.router.navigate(['professor/list']);
    },
    (err: HttpErrorResponse) => {
      this.router.navigate(['professor/list']);
       // this.isLoginError = true;
    });
  }
}
