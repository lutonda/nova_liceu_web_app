import { Component, OnInit } from '@angular/core';
import { Professor } from 'app/Entity/professor';
import { DataService } from 'app/services/data.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-list-professor',
  templateUrl: './list-professor.component.html',
  styleUrls: ['./list-professor.component.scss']
})
export class ListProfessorComponent implements OnInit {

  isLoading = true;
  professors = Array<Professor>()
  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.dataService.get('pedagogy/professor').subscribe((data: any) => {
      this.professors = data;
      this.isLoading = false;
    },
    (err: HttpErrorResponse) => {
       // this.isLoginError = true;
        this.isLoading = false;
    });
  }

}
