import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Student } from 'app/Entity/student';
import { Sex } from 'app/Entity/sex';
import { HttpErrorResponse } from '@angular/common/http';
import { IdCardType } from 'app/Entity/idCardType';
import { Contact } from 'app/Entity/contact';
import { ContactType } from 'app/Entity/contactType';
import { Township } from '../../../Entity/township';

import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Person } from 'app/Entity/person';
@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.scss']
})
export class AddStudentComponent implements OnInit {

  isLoading = true;

  model = new Student()
  contact = new Contact();
  person = new Person();
  sexs = new Array<Sex>();

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;

  contacts: Array<Contact> = [];
  idCardTypes: Array<IdCardType> = new Array<IdCardType>();
  townships: Array<Township> = new Array<Township>();
  contactTypes: Array<ContactType> = new Array<ContactType>();
  bloodGroups: Array<BloodGroup> = new Array<BloodGroup>();

  //
  constructor(private dataService: DataService, private _formBuilder: FormBuilder, private router: Router) {}

  ngOnInit() {

      this.firstFormGroup = this._formBuilder.group({
        firstName: ['', Validators.required],
        middleName: ['', Validators.required],
        lastName: ['', Validators.required],
        sex: ['', Validators.required],
        birthDate: ['', Validators.required],
      });
      this.secondFormGroup = this._formBuilder.group({
        idCard_idCardType: ['', Validators.required],
        idCard_description: ['', Validators.required],
        address_township: ['', Validators.required],
        address_description: ['', Validators.required],
        birthPlace: ['', Validators.required],
      });
      this.thirdFormGroup  = this._formBuilder.group({
        lider: ['', Validators.required]
      });

   this.dataService.get('pedagogy/student/new').subscribe((data: any) => {
      this.townships = data.township;
      this.idCardTypes = data.idCardType;
      this.contactTypes = data.contactType;
      this.bloodGroups = data.bloodGroup;
      this.sexs = data.sex

      this.isLoading = false;
    },
    (err: HttpErrorResponse) => {
       // this.isLoginError = true;
    });
  }

  addContact() {
    this.person.contacts.push(this.contact);
    this.contact = new Contact();
  }
  removeContact(contact) {
    const index = this.person.contacts.indexOf(contact);
    this.person.contacts.splice(index, 1);
  }
  onSubmit() {

    this.person.idCard.idCardNumber = this.secondFormGroup.value.idCard_description;
    this.person.idCard.idCardType = this.secondFormGroup.value.idCard_idCardType;
    this.person.address.description = this.secondFormGroup.value.address_description;
    this.person.address.township = this.secondFormGroup.value.address_township;

    this.model.person = {...this.person, ...this.model, ...this.firstFormGroup.value, ...this.secondFormGroup.value}
    // this.model.period = this.periods.find(perio => perio.id === this.model.period)
    this.dataService.post(this.model, 'pedagogy/student').subscribe((data: any) => {
       this.router.navigate(['student/list']);
    },
    (err: HttpErrorResponse) => {
      this.router.navigate(['student/list']);
       // this.isLoginError = true;
    });
  }
}
