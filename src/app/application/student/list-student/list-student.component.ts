import { Component, OnInit } from '@angular/core';
import { Student } from 'app/Entity/student';
import { DataService } from 'app/services/data.service';
import { HttpErrorResponse } from '../../../../../node_modules_/@angular/common/http';

@Component({
  selector: 'app-list-student',
  templateUrl: './list-student.component.html',
  styleUrls: ['./list-student.component.scss']
})
export class ListStudentComponent implements OnInit {

  students = Array<Student>()
  isLoading: Boolean = true;
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.get('pedagogy/student').subscribe((data: any) => {
      this.students = data;
      this.isLoading = false;
    },
    (err: HttpErrorResponse) => {
       // this.isLoginError = true;
        this.isLoading = false;
    });
  }
  studentCurrentSituation(student: Student) {
    if (student.classes.length > 0) {
        const current = student.classes.find(o => o.academiClass.year === new Date().getFullYear())
        if (current === null) {
          return current.academiClass.number + ' - '  + current.academiClass.grade.number + 'ª Classe'
        }
      }
      return '-';
  }

}
