import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassComponent } from './class/class.component';
import { ListClassComponent } from './class/list-class/list-class.component';
import { AddClassComponent } from './class/add-class/add-class.component';
import { UpdateClassComponent } from './class/update-class/update-class.component';
import { AddStudentComponent } from './student/add-student/add-student.component';
import { ListStudentComponent } from './student/list-student/list-student.component';
import { DetailsStudentComponent } from './student/details-student/details-student.component';
import { UpdateStudentComponent } from './student/update-student/update-student.component';
import { ProfessorComponent } from './professor/professor.component';
import { AddProfessorComponent } from './professor/add-professor/add-professor.component';
import { ListProfessorComponent } from './professor/list-professor/list-professor.component';
import { DetailsProfessorComponent } from './professor/details-professor/details-professor.component';
import { DetailsPersonComponent } from './person/details-person/details-person.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [DetailsPersonComponent],
/*  declarations: [ClassComponent, ListClassComponent, AddClassComponent, UpdateClassComponent],
exports: [ClassComponent, ListClassComponent, AddClassComponent, UpdateClassComponent]*/
})
export class ApplicationModule { }
