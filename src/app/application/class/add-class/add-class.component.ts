

import { Grade } from './../../../Entity/grade';
import { Period } from './../../../Entity/period';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { DataService } from './../../../services/data.service';
import { Component, OnInit } from '@angular/core';
import { AcademiClass } from 'app/Entity/acdemiClass';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Room } from 'app/Entity/room';
import { Subject } from 'app/Entity/subject';
import {MatStepperModule} from '@angular/material/stepper';
import { AlocatedSubject } from 'app/Entity/AlocatedSubject';
import { Professor } from 'app/Entity/professor';
import { Common } from 'app/Enum/Color';
import { Notification } from 'app/components/costum/Notification';

@Component({
  selector: 'app-add-class',
  templateUrl: './add-class.component.html',
  styleUrls: ['./add-class.component.scss']
})
export class AddClassComponent implements OnInit {
  isLoading: Boolean = true;
  model = new AcademiClass();
  alocatedSubject = new AlocatedSubject();
  toppings = new FormControl();

  subjects: Array<Subject> = [];
  rooms: Room[] = [];
  periods: Period[] = [];
  grades: Grade[] = []
  years: string[] = [];
  professors: Professor[] = [];
  // subjects: Subject[] = [];

  //
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;

  //
  constructor(private dataService: DataService, private _formBuilder: FormBuilder, private router: Router) {}

  ngOnInit() {

      this.firstFormGroup = this._formBuilder.group({
        name: ['', Validators.required],
        number: ['', Validators.required],
        period: ['', Validators.required],
        year: ['', Validators.required],
        grade: ['', Validators.required],
        room: ['', Validators.required],
      });
      this.secondFormGroup = this._formBuilder.group({
        secondCtrl: ['', Validators.required]
      });
      this.thirdFormGroup  = this._formBuilder.group({
        lider: ['', Validators.required]
      });

   this.dataService.get('pedagogy/academiclass/new').subscribe((data: any) => {
      this.periods = data.periods;
      this.years = data.years
      this.grades = data.grades
      this.rooms = data.room;
      this.subjects = data.subjects;
      this.professors = data.professors;
      this.isLoading = false;
    },
    (err: HttpErrorResponse) => {
       this.isLoading = true;
    });
  }

  alocatedSubjectSubmit() {
    this.model.subjects.push(this.alocatedSubject);
    this.alocatedSubject = new AlocatedSubject();
  }
  alocatedSubjectRemove(subject) {
    const index = this.model.subjects.indexOf(subject);
    this.model.subjects.splice(index, 1);
  }

  onSubmit() {
    this.isLoading = true;
    this.model = {...this.model, ...this.firstFormGroup.value, ...this.secondFormGroup.value}
    // this.model.period = this.periods.find(perio => perio.id === this.model.period)
    this.dataService.post(this.model, 'pedagogy/academiclass').subscribe((data: any) => {

      new Notification().showNotification('Turma Criada com sucesso', Common.success);
      this.isLoading = false;

      this.router.navigate(['class/list']);
    },
    (err: HttpErrorResponse) => {

      new Notification().showNotification('Erro ao criar a Turma, volte a tentar por favor', Common.danger);
      this.isLoading = false;
      this.router.navigate(['class/list']);
       // this.isLoginError = true;
    });
  }
}
