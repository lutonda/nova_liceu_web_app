import { IdCardType } from './idCardType';

export class IdCard {
  public id: string;
  public idCardNumber: string;
  public idCardType: IdCardType = new IdCardType;
  public idCardTypeid: string

  constructor() {}
}
