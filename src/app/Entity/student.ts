import { Person } from './person';
import { RegistrationClass } from './registrationClass';
export class Student {
  public id: string;
  public code: string;
  public person: Person = new Person;
  public classes: Array<RegistrationClass> = new Array<RegistrationClass>();
  constructor() {}
}
