import { AcademiClass } from './acdemiClass';
import { Subject } from './subject';
import { Professor } from './professor';

export class AlocatedSubject {

    public id: string;

    public class: AcademiClass;

    public subject: Subject;

    public professor: Professor;

    constructor() {}
}
