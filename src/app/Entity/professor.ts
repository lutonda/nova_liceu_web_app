import { Person } from './person';
import { AlocatedSubject } from './AlocatedSubject';

export class Professor {

    public id: string;

    public person: Person = new Person();

    public subjects: Array<AlocatedSubject> = new Array<AlocatedSubject>() ;
    constructor() {}
}
